// Maedeh Hassani (1942575)
public class BikeStore {
    public static void main(String[] args) {

        final int ARRAY_SIZE = 4;
        Bicycle[] myBic = new Bicycle[ARRAY_SIZE];

        myBic[0] = new Bicycle("Manufacturer00", 0, 10);

        myBic[1] = new Bicycle("Manufacturer01", 1, 11);

        myBic[2] = new Bicycle("Manufacturer02", 2, 12);

        myBic[3] = new Bicycle("Manufacturer03", 3, 13);

        for (int i = 0; i < ARRAY_SIZE; i++) {
            System.out.println("Bicycle Num: " + i + "\n" + myBic[i]);
            System.out.println("----------------------------------------------");
        }

    }
}